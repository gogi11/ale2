﻿using ALE2.Nodes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Nodes {
    [TestClass]
    public class TestNodeConnection {
        [TestMethod]
        public void TestToStringNfaAndDfa() {
            List<NodeConnection> conns = new List<NodeConnection>();
            string connectionLetters = "abcdef_";
            for (int i = 0; i < connectionLetters.Length; i++) {
                conns.Add(new NodeConnection(new Node("A"), new Node("B"), connectionLetters[i] + ""));
            }

            for (int i = 0; i < conns.Count - 1; i++) {
                string label = (connectionLetters[i] + "").Replace("_", "ε");
                Assert.AreEqual(conns[i].ToString(), $"\"A\" -> \"B\" [label=\"{label}\"]\n");
            }
        }
        [TestMethod]
        public void TestToStringPda() {
            List<NodeConnection> conns = new List<NodeConnection>();
            string connectionLetters = "abcdef_";
            string stackIn = "_a_bcd_";
            string stackOut = "b__cc_a";
            for (int i = 0; i < connectionLetters.Length; i++) {
                conns.Add(new NodeConnection(new Node("A"), new Node("B"), connectionLetters[i] + "", stackIn[i] + "", stackOut[i] + ""));
            }

            for (int i = 0; i < conns.Count; i++) {
                string label = $"{connectionLetters[i]}[{stackIn[i] }, {stackOut[i]}]".Replace("_", "ε");
                if (stackIn[i] + "" == "_" && stackOut[i] + "" == "_") {
                    label = connectionLetters[i] + "";
                }
                Assert.AreEqual(conns[i].ToString(), $"\"A\" -> \"B\" [label=\"{label}\"]\n");
            }
        }
        [TestMethod]
        public void TestGetVal() {
            List<NodeConnection> conns = new List<NodeConnection>();
            string connectionLetters = "abcdef_";
            for (int i = 0; i < connectionLetters.Length; i++) {
                conns.Add(new NodeConnection(new Node("A"), new Node("B"), connectionLetters[i] + ""));
            }

            for (int i = 0; i < conns.Count; i++) {
                Assert.AreEqual(conns[i].GetVal(), (connectionLetters[i]+"").Replace("_", ""));
            }
        }
        [TestMethod]
        public void TestGetLabelNfaDfa() {
            List<NodeConnection> conns = new List<NodeConnection>();
            string connectionLetters = "abcdef_";
            for (int i = 0; i < connectionLetters.Length; i++) {
                conns.Add(new NodeConnection(new Node("A"), new Node("B"), connectionLetters[i] + ""));
            }

            for (int i = 0; i < conns.Count; i++) {
                Assert.AreEqual(conns[i].GetLabel(), (connectionLetters[i]+""));
            }
        }
        [TestMethod]
        public void TestGetLabelPda() {
            List<NodeConnection> conns = new List<NodeConnection>();
            string connectionLetters = "abcdef_";
            string stackIn = "_a_bcd_";
            string stackOut = "b__cc_a";
            for (int i = 0; i < connectionLetters.Length; i++) {
                conns.Add(new NodeConnection(new Node("A"), new Node("B"), connectionLetters[i] + "", stackIn[i] + "", stackOut[i] + ""));
            }

            for (int i = 0; i < conns.Count; i++) {
                string label = $"{connectionLetters[i]}[{stackIn[i] }, {stackOut[i]}]";
                if (stackIn[i] + "" == "_" && stackOut[i] + "" == "_") {
                    label = connectionLetters[i] + "";
                }
                Assert.AreEqual(conns[i].GetLabel(), label);
            }
        }
    }
}
