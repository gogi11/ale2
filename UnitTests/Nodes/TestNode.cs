﻿using ALE2.Nodes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Nodes {
    [TestClass]
    public class TestNode {
        [TestMethod]
        public void TestNewNodesAreNotFinal() {
            Node n = new Node("");
            Assert.IsFalse(n.IsFinal);
        }
        [TestMethod]
        public void TestMakeNodeFinal() {
            Node n = new Node("", false);
            Assert.IsFalse(n.IsFinal);
            n.MakeNodeFinal();
            Assert.IsTrue(n.IsFinal);
        }
        [TestMethod]
        public void TestAddConnection() {
            Node n = new Node("", false);
            NodeConnection conn = new NodeConnection(n, n);
            Assert.AreEqual(n.Connections.Count, 0);

            n.AddConnection(conn);
            Assert.AreEqual(n.Connections.Count, 1);
            Assert.AreEqual(n.Connections[0], conn);
        }
        [TestMethod]
        public void TestToStringMethod() {
            Node n = new Node("A", false);
            Node m = new Node("A", true);

            Assert.AreEqual(n.ToString(), "\"A\"[shape = circle]\n");
            Assert.AreEqual(m.ToString(), "\"A\"[shape = doublecircle]\n");
        }
        [TestMethod]
        public void TestGetAllEndNodesConnectedToThisNode() {
            Node n = new Node("A", false);
            string names = "BCDEFGHIJKLMN";
            string connections = "aa_bb_cc_defg";
            Node[] endNodes = new Node[names.Length];
            for(int i=0; i<names.Length; i++) {
                endNodes[i] = new Node(names[i] + "");
                n.AddConnection(new NodeConnection(n, endNodes[i], connections[i] + ""));
            }
            List<Node> result;

            result = n.GetAllEndNodesConnectedToThisNode("a");
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0], endNodes[0]);
            Assert.AreEqual(result[1], endNodes[1]);
            
            result = n.GetAllEndNodesConnectedToThisNode("b");
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0], endNodes[3]);
            Assert.AreEqual(result[1], endNodes[4]);
            
            result = n.GetAllEndNodesConnectedToThisNode("c");
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0], endNodes[6]);
            Assert.AreEqual(result[1], endNodes[7]);
            
            result = n.GetAllEndNodesConnectedToThisNode("_");
            Assert.AreEqual(result.Count, 3);
            Assert.AreEqual(result[0], endNodes[2]);
            Assert.AreEqual(result[1], endNodes[5]);
            Assert.AreEqual(result[2], endNodes[8]);

            for (int i=0; i< 4; i++) {
                result = n.GetAllEndNodesConnectedToThisNode(connections[9+i]+"");
                Assert.AreEqual(result.Count, 1);
                Assert.AreEqual(result[0], endNodes[9+i]);
            }
        }
    }
}
