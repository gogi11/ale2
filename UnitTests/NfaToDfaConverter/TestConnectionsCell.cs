﻿using ALE2;
using ALE2.Nodes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.NfaToDfaConverter {
    [TestClass]
    public class TestConnectionsCell {
        [TestMethod]
        public void TestAddNode() {
            ConnectionsCell c = new ConnectionsCell(new List<Node>());
            Node n = new Node("n");

            Assert.AreEqual(c.Nodes.Count, 0);
            c.AddNode(n);
            Assert.AreEqual(c.Nodes.Count, 1);
            Assert.AreEqual(c.Nodes[0], n);
            c.AddNode(n);
            Assert.AreEqual(c.Nodes.Count, 1);
            Assert.AreEqual(c.Nodes[0], n);
        }
        [TestMethod]
        public void TestAddNodesFromCell() {
            Node n1 = new Node("n1");
            Node n2 = new Node("n2");
            Node n3 = new Node("n3");
            ConnectionsCell c1 = new ConnectionsCell(new List<Node>() { n1});
            ConnectionsCell c2 = new ConnectionsCell(new List<Node>() { n2, n3});

            Assert.AreEqual(c1.Nodes.Count, 1);
            c1.AddNodesFromCell(c2);
            Assert.AreEqual(c1.Nodes.Count, 3);
            Assert.AreEqual(c1.Nodes[0], n1);
            Assert.AreEqual(c1.Nodes[1], n2);
            Assert.AreEqual(c1.Nodes[2], n3);
            c1.AddNodesFromCell(c1);
            Assert.AreEqual(c1.Nodes.Count, 3);
            Assert.AreEqual(c1.Nodes[0], n1);
            Assert.AreEqual(c1.Nodes[1], n2);
            Assert.AreEqual(c1.Nodes[2], n3);
        }

        [TestMethod]
        public void TestIsCellFinal() {
            Node n1 = new Node("n1");
            Node n2 = new Node("n2");
            Node n3 = new Node("n3");
            Node n4 = new Node("n4", true);
            Node n5 = new Node("n5");
            ConnectionsCell not_final = new ConnectionsCell(new List<Node>() { n1, n2, n3});
            ConnectionsCell final = new ConnectionsCell(new List<Node>() { n4, n5});
            Assert.IsFalse(not_final.IsCellFinal());
            Assert.IsTrue(final.IsCellFinal());
        }
        [TestMethod]
        public void TestToStringWorksAndALwaysSorts() {
            Node n1 = new Node("n1");
            Node n2 = new Node("n2");
            Node n3 = new Node("n3");
            ConnectionsCell c = new ConnectionsCell(new List<Node>() { n2, n3, n1});

            Assert.AreEqual(c.ToString(), "n1,n2,n3");
        }

        [TestMethod]
        public void TestEquals() {
            Node n1 = new Node("n1");
            Node n2 = new Node("n2");
            Node n3 = new Node("n3");
            ConnectionsCell c1 = new ConnectionsCell(new List<Node>() { n2, n3, n1 });
            ConnectionsCell c2 = new ConnectionsCell(new List<Node>() { n3, n2, n1 });

            Assert.AreEqual(c1, c2);
            Assert.AreNotEqual(c1.GetHashCode(), c2.GetHashCode());
        }
    }
}
