﻿using ALE2;
using ALE2.Nodes;
using ALE2.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.NfaToDfaConverter {
    [TestClass]
    public class TestConnectionsTable {
        private string path = "../../../automatons/";
        [TestMethod]
        public void TestToString() {
            List<string> alphabet = "abc".ToList().Select(letter => letter+"").ToList();
            List<ConnectionsRow> rows = new List<ConnectionsRow>();
            Node A = new Node("A");
            Node B = new Node("B");
            Node C = new Node("C");

            rows.Add(new ConnectionsRow(A, new List<ConnectionsCell>() { new ConnectionsCell(new List<Node>(){ A, B}), null, null}));
            rows.Add(new ConnectionsRow(B, new List<ConnectionsCell>() { new ConnectionsCell(new List<Node>(){ A }), new ConnectionsCell(new List<Node>() { B }), new ConnectionsCell(new List<Node>() { C }) }));
            rows.Add(new ConnectionsRow(C, new List<ConnectionsCell>() { new ConnectionsCell(new List<Node>()) , new ConnectionsCell(new List<Node>() { A, B, C }), new ConnectionsCell(new List<Node>())}));

            ConnectionsTable ct = new ConnectionsTable(rows, alphabet);
            Assert.AreEqual(ct.ToString(), "\ta\tb\tc\nA\tA,B\t\t\nB\tA\tB\tC\nC\t_\tA,B,C\t_\n");
        }
        [TestMethod]
        public void TestCreatingTableWithAutomatonCreatesCorrectTable() {
            Automaton a = AutomatonReader.ReadFile(path + "finite.txt");
            ConnectionsTable ct = new ConnectionsTable(a);
            Assert.AreEqual(ct.ToString(), "\ta\tb\tc\t_\nZ\tA\tB\t_\tB\nA\t_\t_\t_\t_\nB\t_\t_\t_\t_\n");
        }

        [TestMethod]
        public void TestGetCell() {
            Automaton a = AutomatonReader.ReadFile(path + "finite.txt");
            ConnectionsTable ct = new ConnectionsTable(a);
            List<string> alphabet = a.Alphabet;
            List<Node> startingNodes = a.AllNodes;
            
            foreach(string letter in alphabet) {
                foreach(Node node in startingNodes) {
                    ConnectionsCell c = ct.GetCell(node, letter);
                    List<Node> allNodesWithLetter = node.GetAllEndNodesConnectedToThisNode(letter);
                    foreach(Node cellNode in c.Nodes) {
                        Assert.IsTrue(allNodesWithLetter.Contains(cellNode));
                        allNodesWithLetter.Remove(cellNode);
                    }
                    Assert.AreEqual(allNodesWithLetter.Count, 0);
                }
            }
        }
    }
}
