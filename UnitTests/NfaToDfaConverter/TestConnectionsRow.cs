﻿using ALE2;
using ALE2.Nodes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.NfaToDfaConverter {
    [TestClass]
    public class TestConnectionsRow {
        [TestMethod]
        public void TestToString() {
            Node A = new Node("A");
            Node B = new Node("B");
            Node C = new Node("C");
            ConnectionsRow row = new ConnectionsRow(B, new List<ConnectionsCell>() { new ConnectionsCell(new List<Node>() { A }), new ConnectionsCell(new List<Node>() { B }), new ConnectionsCell(new List<Node>() { C }) });
            Assert.AreEqual(row.ToString(), "B\tA\tB\tC");
        }
        [TestMethod]
        public void TestGetCell() {
            Node A = new Node("A");
            Node B = new Node("B");
            Node C = new Node("C");
            ConnectionsRow row = new ConnectionsRow(B, new List<ConnectionsCell>() { new ConnectionsCell(new List<Node>() { A }), new ConnectionsCell(new List<Node>() { B }), new ConnectionsCell(new List<Node>() { C }) });
            
            Assert.AreEqual(row.GetCell(0).Nodes.Count, 1);
            Assert.AreEqual(row.GetCell(0).Nodes[0], A);
            Assert.AreEqual(row.GetCell(1).Nodes.Count, 1);
            Assert.AreEqual(row.GetCell(1).Nodes[0], B);
            Assert.AreEqual(row.GetCell(2).Nodes.Count, 1);
            Assert.AreEqual(row.GetCell(2).Nodes[0], C);
        }
    }
}
