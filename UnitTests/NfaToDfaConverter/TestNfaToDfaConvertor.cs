﻿using ALE2;
using ALE2.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests {
    [TestClass]
    public class TestNfaToDfaConvertor {
        private string path = "../../../automatons/";
        [TestMethod]
        public void TestConvertionWorks() {
            Automaton a = NFAReader.FindAutomaton("*(|(a,b))", "yes.txt");
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsTrue(a.IsStringPossible("aaaaab"));
            Assert.IsTrue(a.IsStringPossible("babababbbaba"));
            Assert.IsTrue(a.IsStringPossible("abab"));
            Assert.IsTrue(a.IsStringPossible("bbbbb"));
            Assert.IsFalse(a.IsStringPossible("bbbbbc"));
            Assert.IsFalse(a.IsStringPossible("c"));
        }

        [TestMethod]
        public void TestConvertionRemovesEmptyStringFromPossibleWords() {
            Automaton a = NFAReader.FindAutomaton("*(|(a,b))", "yes.txt");
            Assert.IsTrue(a.IsStringPossible(""));

            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsFalse(a.IsStringPossible(""));
        }

        [TestMethod]
        public void TestConvertionIfRepeatedStillWorks() {
            Automaton a = NFAReader.FindAutomaton("*(|(a,b))", "yes.txt");
            Automaton b = null;
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            for (int i=0; i<10; i++) { 
                b = ALE2.NfaToDfaConverter.ConvertToDFA(a);
                Assert.AreEqual(a.AllNodes.Count, b.AllNodes.Count);
                Assert.AreEqual(a.AllConnections.Count, b.AllConnections.Count);
                a = b;
            }
            Assert.IsTrue(a.IsStringPossible("aaaaab"));
            Assert.IsTrue(a.IsStringPossible("babababbbaba"));
            Assert.IsTrue(a.IsStringPossible("abab"));
            Assert.IsTrue(a.IsStringPossible("bbbbb"));
            Assert.IsFalse(a.IsStringPossible("bbbbbc"));
            Assert.IsFalse(a.IsStringPossible("c"));
        }

        [TestMethod]
        public void TestCustomNFAWithSinkCanConvertToDFA() {
            Automaton a = AutomatonReader.ReadFile(path+ "finite_but_strange.txt");
            Assert.IsTrue(a.IsStringPossible("0"));
            Assert.IsFalse(a.IsStringPossible("01"));
            Assert.IsFalse(a.IsStringPossible("00"));
            Assert.IsFalse(a.IsStringPossible("1"));
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsTrue(a.IsStringPossible("0"));
            Assert.IsFalse(a.IsStringPossible("01"));
            Assert.IsFalse(a.IsStringPossible("00"));
            Assert.IsFalse(a.IsStringPossible("1"));
        }


        [TestMethod]
        public void TestConvertionOfAutomatonWithOnly1TransitionWorks() {
            Automaton a = AutomatonReader.ReadFile(path + "first_word_isnt_empty.txt");
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsFalse(a.IsStringPossible(""));
            Assert.IsFalse(a.IsStringPossible("a"));
            Assert.IsFalse(a.IsStringPossible("aaaaa"));
            Assert.IsFalse(a.IsStringPossible("aa"));
        }

        [TestMethod]
        public void TestConvertionOfAutomatonWithLanguageSingleLetterWorks() {
            Automaton a = NFAReader.FindAutomaton("a", "yes.txt");
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsFalse(a.IsStringPossible(""));
            Assert.IsFalse(a.IsStringPossible("aaaaa"));
            Assert.IsFalse(a.IsStringPossible("aa"));
            Assert.IsFalse(a.IsStringPossible("b"));

            List<string> allWords = a.GetAllWords();

            Assert.AreEqual(a.AllNodes.Count, 3);
            Assert.AreEqual(allWords.Count, 1);
            Assert.AreEqual(allWords[0], "a");
            Assert.IsTrue(a.IsDFA());
        }

        [TestMethod]
        public void TestConvertionOfAutomatonWithLanguageWithOnlyConcatWorks() {
            Automaton a = NFAReader.FindAutomaton(".(b,.(a,a))", "yes.txt");
            a = ALE2.NfaToDfaConverter.ConvertToDFA(a);
            Assert.IsFalse(a.IsStringPossible(""));
            Assert.IsTrue(a.IsStringPossible("baa"));
            Assert.IsFalse(a.IsStringPossible("aba"));
            Assert.IsFalse(a.IsStringPossible("aab"));
            Assert.IsFalse(a.IsStringPossible("bba"));
            Assert.IsFalse(a.IsStringPossible("aabbb"));

            List<string> allWords = a.GetAllWords();

            Assert.AreEqual(a.AllNodes.Count, 5);
            Assert.AreEqual(allWords.Count, 1);
            Assert.AreEqual(allWords[0], "baa");
            Assert.IsTrue(a.IsDFA());
        }

        [TestMethod]
        public void TestConvertionOfAutomatonWithStepsBetween() {
            Automaton a = NFAReader.FindAutomaton("*(|(a,b))", "yes.txt");
            (ConnectionsTable t1, ConnectionsTable t2) = (null, null);
            (a, t1, t2) = ALE2.NfaToDfaConverter.ConvertToDFAWithStepsBetween(a);

            Assert.AreEqual(t1.Rows.Count, 8);
            Assert.AreEqual(t1.Alphabet.Count, 3);
            for (int i = 0; i < 8; i++) { 
                Assert.AreEqual(t1.Rows[i].StartingCell.ToString(), "n"+i);
            }

            Assert.AreEqual(t2.Rows.Count, 3);
            Assert.AreEqual(t2.Alphabet.Count, 2);
            Assert.AreEqual(t2.Rows[0].StartingCell.ToString(), "n0,n1,n2,n4,n6");
            Assert.AreEqual(t2.Rows[1].StartingCell.ToString(), "n1,n2,n3,n4,n5,n6");
            Assert.AreEqual(t2.Rows[2].StartingCell.ToString(), "n1,n2,n3,n4,n6,n7");
        }
    }
}
