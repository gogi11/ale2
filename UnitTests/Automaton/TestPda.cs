﻿using System;
using System.IO;
using ALE2;
using ALE2.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {
    [TestClass]
    public class TestPda {
        private string path = "../../../automatons/";
        [TestMethod]
        public void TestPDAAutomatonWorks() {
            Automaton a = AutomatonReader.ReadFile(path + "pda.txt");
            Assert.AreEqual(a.Config.Stack.Count, 1);
            Assert.AreEqual(a.Config.Stack.Peek(), "x");
            Assert.IsTrue(a.IsStringPossible("aacc"));
            Assert.IsTrue(a.IsStringPossible("bbcc"));
            Assert.IsTrue(a.IsStringPossible("abcc"));
            Assert.IsFalse(a.IsStringPossible("aaccc"));
            Assert.IsFalse(a.IsStringPossible("aaccc"));
            Assert.IsFalse(a.IsStringPossible("baccc"));
        }
        [TestMethod]
        public void TestPDAAutomatonArithmeticWorks() {
            Automaton a = AutomatonReader.ReadFile(path + "pdaToAcceptArithmetic.txt");
            Assert.AreEqual(a.Config.Stack.Count, 1);
            Assert.AreEqual(a.Config.Stack.Peek(), "x");
            Assert.IsTrue(a.IsStringPossible("1+2"));
            Assert.IsTrue(a.IsStringPossible("9*4"));
            Assert.IsTrue(a.IsStringPossible("1*(8/6+7)-5"));
            Assert.IsTrue(a.IsStringPossible("11"));
            Assert.IsTrue(a.IsStringPossible("22*33"));
            Assert.IsTrue(a.IsStringPossible("(22*33)/2"));
            Assert.IsFalse(a.IsStringPossible("13(83*"));
            Assert.IsFalse(a.IsStringPossible("13)83*"));
        }

        [TestMethod]
        public void TestPDAIsPDA() {
            string[] fileEntries = Directory.GetFiles(path);
            foreach (string fileName in fileEntries) {
                Automaton a = AutomatonReader.ReadFile(fileName);
                if (fileName.Contains("pda")) {
                    Assert.IsTrue(a.IsPDA());
                }
            }
        }
    }
}
