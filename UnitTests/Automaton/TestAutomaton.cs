﻿using ALE2;
using ALE2.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UnitTests {
    [TestClass]
    public class TestAutomaton {
        private string path = "../../../automatons/";
        [TestMethod]
        public void TestExampleAutomatonIsStringPossibleWorks() {
            Automaton a = AutomatonReader.ReadFile(path+"example.txt");

            // true
            Assert.IsTrue(a.IsStringPossible("bb"));
            Assert.IsTrue(a.IsStringPossible("bbbbbbbbbbb"));
            Assert.IsTrue(a.IsStringPossible("b"));
            Assert.IsTrue(a.IsStringPossible(""));
            Assert.IsTrue(a.IsStringPossible("a"));

            // false
            Assert.IsFalse(a.IsStringPossible("aa"));
            Assert.IsFalse(a.IsStringPossible("cc"));
            Assert.IsFalse(a.IsStringPossible("ab"));
        }

        [TestMethod]
        public void TestInfiniteAutomatonAllWordsReturnsNull() {
            Console.WriteLine("Running");
            Automaton a = AutomatonReader.ReadFile(path + "example.txt");
            Assert.IsNull(a.GetAllWords());
        }

        [TestMethod]
        public void TestFiniteAutomatonAllWordsReturns3CorrectWords() {
            Automaton a = AutomatonReader.ReadFile(path + "finite.txt");
            List<string> words = a.GetAllWords();
            Assert.AreEqual(3, words.Count);
            Assert.IsTrue(words.Contains(""));
            Assert.IsTrue(words.Contains("a"));
            Assert.IsTrue(words.Contains("b"));
        }

        [TestMethod]
        public void TestNFAAutomatonIsNotDFA() {
            Automaton a = AutomatonReader.ReadFile(path + "nfa.txt");
            Assert.IsFalse(a.IsDFA());
        }
        [TestMethod]
        public void TestDFAAutomatonIsDFA() {
            Automaton a = AutomatonReader.ReadFile(path + "dfa.txt");
            Assert.IsTrue(a.IsDFA());
        }

        [TestMethod]
        public void TestAllTestAutomatons() {
            string[] fileEntries = Directory.GetFiles(path);
            foreach (string fileName in fileEntries) {
                Console.WriteLine(fileName);
                Automaton a = AutomatonReader.ReadFile(fileName);
                Assert.IsTrue(TristateHandler.TristateToBool(a.Config.TestIsDfa) == a.IsDFA());
                List<string> allWordsPossible = a.GetAllWords();
                Assert.IsTrue(TristateHandler.TristateToBool(a.Config.TestIsFinite) == (allWordsPossible != null)); 
                if(a.Config.TestWords != null) { 
                    foreach (var kv in a.Config.TestWords) {
                        Assert.IsTrue(kv.Value == a.IsStringPossible(kv.Key));
                    }
                }
            }
        }

        [TestMethod]
        public void TestAllTestAutomatonsSaveToFile() {
            string[] fileEntries = Directory.GetFiles(path);
            foreach (string fileName in fileEntries) {
                Automaton a = AutomatonReader.ReadFile(fileName);
                a.SaveToFile("yes.txt");
                List<string> new_text = File.ReadAllLines("yes.txt").Select(str => str.Replace(" ", "")).ToList();
                List<string> original_text = File.ReadAllLines(fileName).Select(str => str.Replace(" ", "")).ToList();
                foreach(string s in new_text) {
                    Assert.IsTrue(original_text.Contains(s));
                }
            }
        }

        [TestMethod]
        public void TestFirstInputDoesntAllowEmptyString() {
            Automaton a = AutomatonReader.ReadFile(path + "first_word_isnt_empty.txt");
            Assert.IsFalse(a.IsStringPossible(""));
        }


        [TestMethod]
        public void TestNFAAndDFAArentPDA() {
            string[] fileEntries = Directory.GetFiles(path);
            foreach (string fileName in fileEntries) {
                Automaton a = AutomatonReader.ReadFile(fileName);
                if (!fileName.ToLower().Contains("pda")) {
                    Console.WriteLine(a.Config.Stack);
                    Assert.IsFalse(a.IsPDA());
                }
            }
        }

        [TestMethod]
        public void TestEmptyLoopsAreFinite() {
            Automaton a = AutomatonReader.ReadFile(path + "nfaEmptyLoops.txt");
            Assert.IsTrue(a.IsStringPossible(""));
            List<string> allWords = a.GetAllWords();
            Assert.IsTrue(allWords != null);
            Assert.AreEqual(allWords.Count, 1);
            Assert.AreEqual(allWords[0], "");
        }
    }
}
