﻿using ALE2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests {
    [TestClass]
    public class TestTristateEnumAndHandler {
        [TestMethod]
        public void TestTriToBooleanWorks() {
            Assert.IsFalse(TristateHandler.TristateToBool(TristateEnum.FALSE));
            Assert.IsTrue(TristateHandler.TristateToBool(TristateEnum.TRUE));
            try {
                TristateHandler.TristateToBool(TristateEnum.DOESNT_EXIST);
                Assert.Fail();
            } catch (Exception e) {
                Assert.AreEqual(e.Message, "The object doesn't exist!");
            }
        }
        [TestMethod]
        public void TestBoolToTriWorks() {
            Assert.AreEqual(TristateHandler.BoolToTristate(false), TristateEnum.FALSE);
            Assert.AreEqual(TristateHandler.BoolToTristate(true), TristateEnum.TRUE);
        }
        [TestMethod]
        public void TestTriAndTriOperatorReturnsDoesntExistIfEitherDoesntExist() {
            TristateEnum d = TristateEnum.DOESNT_EXIST;
            TristateEnum t = TristateEnum.TRUE;
            TristateEnum f = TristateEnum.FALSE;
            Assert.AreEqual(TristateHandler.TriAndTriOperator(d, d), d);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(d, t), d);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(t, d), d);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(d, f), d);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(f, d), d);
        }
        [TestMethod]
        public void TestTriAndTriOperatorReturnsTrueIfTrue() {
            TristateEnum t = TristateEnum.TRUE;
            Assert.AreEqual(TristateHandler.TriAndTriOperator(t, t), t);
        }
        [TestMethod]
        public void TestTriAndTriOperatorReturnsFalseIfFalse() {
            TristateEnum t = TristateEnum.TRUE;
            TristateEnum f = TristateEnum.FALSE;
            Assert.AreEqual(TristateHandler.TriAndTriOperator(f, f), f);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(f, t), f);
            Assert.AreEqual(TristateHandler.TriAndTriOperator(t, f), f);
        }


        [TestMethod]
        public void TestBoolAndTriOperatorReturnsDoesntExistIfTriDoesntExist() {
            TristateEnum d = TristateEnum.DOESNT_EXIST;
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(true, d), d);
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(false, d), d);
        }
        [TestMethod]
        public void TestBoolAndTriOperatorReturnsTrueIfTrue() {
            TristateEnum t = TristateEnum.TRUE;
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(true, t), t);
        }
        [TestMethod]
        public void TestBoolAndTriOperatorReturnsFalseIfFalse() {
            TristateEnum t = TristateEnum.TRUE;
            TristateEnum f = TristateEnum.FALSE;
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(false, f), f);
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(false, t), f);
            Assert.AreEqual(TristateHandler.BoolAndTriOperator(true, f), f);
        }
    }
}
