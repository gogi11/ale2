﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ALE2;
using ALE2.Readers;
using ALE2.Nodes;
using System.Collections.Generic;

namespace UnitTests.Readers {
    [TestClass]
    public class TestAutomatonReader {
        string path = "../../../automatons/incorrect/";
        string pathCorrect = "../../../automatons/";
        Automaton a;
        List<Node> nodes;
        List<NodeConnection> connections;


        [TestInitialize]
        public void SetUp() {
            a = AutomatonReader.ReadFile(pathCorrect + "example.txt");
            nodes = a.AllNodes;
            connections = a.AllConnections;
        }

        [TestMethod]
        public void TestReadFileReturnsAutomatonWithCorrectStates() {
            Assert.AreEqual(3, nodes.Count);
            Assert.AreEqual("Z", nodes[0].Name);
            Assert.AreEqual("A", nodes[1].Name);
            Assert.AreEqual("B", nodes[2].Name);
            Assert.IsFalse(nodes[0].IsFinal);
            Assert.IsTrue(nodes[1].IsFinal);
            Assert.IsTrue(nodes[2].IsFinal);
        }

        [TestMethod]
        public void TestReadFileReturnsAutomatonWithCorrectConnectionValues() {
            Assert.AreEqual(4, connections.Count);
            Assert.AreEqual("a", connections[0].Value);
            Assert.AreEqual("b", connections[1].Value);
            Assert.AreEqual("_", connections[2].Value);
            Assert.AreEqual("b", connections[3].Value);
        }

        [TestMethod]
        public void TestReadFileReturnsAutomatonWithCorrectConnectionStartAndEndNodes() {
            Assert.AreEqual(nodes[0], connections[0].StartNode);
            Assert.AreEqual(nodes[0], connections[1].StartNode);
            Assert.AreEqual(nodes[0], connections[2].StartNode);
            Assert.AreEqual(nodes[2], connections[3].StartNode);

            Assert.AreEqual(nodes[1], connections[0].EndNode);
            Assert.AreEqual(nodes[2], connections[1].EndNode);
            Assert.AreEqual(nodes[2], connections[2].EndNode);
            Assert.AreEqual(nodes[2], connections[3].EndNode);
        }

        [TestMethod]
        public void TestReadFileWithIncorrectWordYesNoValueThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "automaton_incorrect_word.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You need to specify the word followed by either \"y\" or \"n\"! You specified "));
            }
        }

        [TestMethod]
        public void TestReadFileWithTwoSameWordsThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "automaton_double_word.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.AreEqual(e.Message, "Please specify each word only once!");
            }
        }

        [TestMethod]
        public void TestReadFileWithTransitionOutsideOfAlphabetThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "not_in_alph.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You haven't specified this letter"));
            }
        }

        [TestMethod]
        public void TestReadFileWithStackTransitionInAndOutsideOfStackThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "incorrect_stack_out.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You haven't specified this stack letter! "));
            }
            try {
                Automaton a = AutomatonReader.ReadFile(path + "incorrect_stack_in.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You haven't specified this stack letter! "));
            }
        }

        [TestMethod]
        public void TestReadFileWithStateDefinedTwiceThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "double_state.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You can only have 1 state with that name!"));
            }
        }

        [TestMethod]
        public void TestReadFileWithStateNotDefinedButUsedTwiceThrowsException() {
            try {
                Automaton a = AutomatonReader.ReadFile(path + "state_finite_undefined.txt");
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("Incorrect input! You haven't specified this state!"));
                Assert.IsTrue(e.Message.Contains("Name: C"));
            }
        }

    }
}
