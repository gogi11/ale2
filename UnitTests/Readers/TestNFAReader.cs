﻿using ALE2;
using ALE2.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Readers {
    [TestClass]
    public class TestNFAReader {
        private string fileName = "yes.txt";
        private string exp = "|(*(|(a,.(b,c))),c)";
        private string notValidExp = "*(a,c)";
        [TestMethod]
        public void TestNFAReaderCreatesFileAndFirstLineContainsExpression() {
            Automaton a = NFAReader.FindAutomaton(exp, fileName);
            Assert.IsTrue(File.Exists(fileName));
            StreamReader file = new StreamReader(fileName);
            Assert.IsTrue(file.ReadLine().Contains(exp));
            file.Close();
        }
        [TestMethod]
        public void TestNFAReaderCreatesCorrectAutomata() {
            Automaton a = NFAReader.FindAutomaton(exp, fileName);
            // true
            Assert.IsTrue(a.IsStringPossible(""));
            Assert.IsTrue(a.IsStringPossible("a"));
            Assert.IsTrue(a.IsStringPossible("bc"));
            Assert.IsTrue(a.IsStringPossible("aa"));
            Assert.IsTrue(a.IsStringPossible("bcbc"));
            Assert.IsTrue(a.IsStringPossible("abc"));
            Assert.IsTrue(a.IsStringPossible("bca"));
            Assert.IsTrue(a.IsStringPossible("c"));
            // false
            Assert.IsFalse(a.IsStringPossible("cc"));
            Assert.IsFalse(a.IsStringPossible("abcc"));
            Assert.IsFalse(a.IsStringPossible("ac"));
            Assert.IsFalse(a.IsStringPossible("bcc"));
        }
        [TestMethod]
        public void TestNFAReaderWithNotValidExpressionRaisesError() {
            try { 
                NFAReader.FindAutomaton(notValidExp, fileName);
                Assert.Fail();
            } catch (Exception e) {
                Assert.IsTrue(e.Message.Contains("This part of the expression is incorrect: "));
            }
        }
    }
}
