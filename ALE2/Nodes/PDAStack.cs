﻿using ALE2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Nodes {
    public class PDAStack: IPDAStack {
        private Stack<string> stack;
        public int Count { get { return stack.Count; } }

        public PDAStack(Stack<string> stack = null) {
            stack = stack ?? new Stack<string>();
            this.stack = stack;
        }

        public string PeekStack() {
            if (stack.Count > 0) {
                return stack.Peek();
            }
            return "_";
        }
        public PDAStack CloneStack() {
            if (stack == null) {
                return null;
            }
            var arr = new string[stack.Count];
            stack.CopyTo(arr, 0);
            Array.Reverse(arr);
            return new PDAStack(new Stack<string>(arr));
        }
        public void PopStack(string symbol) {
            if (stack.Count > 0) {
                if (symbol != "_") {
                    stack.Pop();
                }
            }
        }
        public void PushStack(string symbol) {
            if (symbol != "_") {
                stack.Push(symbol);
            }
        }
    }
}
