﻿using ALE2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Nodes {
    public class NodeConnection: INodeConnection {
        public Node StartNode { get; private set; }
        public Node EndNode { get; private set; }
        public string Value { get; private set; }
        public string StackIn { get; private set; }
        public string StackOut { get; private set; }

        public NodeConnection(Node start, Node end, string val = "_", string StackIn = "_", string StackOut = "_") {
            this.StartNode = start;
            this.EndNode = end;
            this.Value = val;
            this.StackIn = StackIn;
            this.StackOut = StackOut;
        }


        override public string ToString() {
            return $"\"{this.StartNode.Name}\" -> \"{this.EndNode.Name}\" [label=\"{GetLabel().Replace("_", "ε")}\"]\n";
        }

        public string GetVal() {
            return this.Value == "_" ? "" : this.Value;
        }
        public string GetLabel() {
            string label = this.Value;    
            if (this.StackIn != "_" || this.StackOut != "_") {
                string sin = this.StackIn;
                string sout = this.StackOut;
                return $"{label}[{sin}, {sout}]";
            }
            return label;
        }
    }
}
