﻿using ALE2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Nodes {
    public class Node: INode {
        public string Name { get; private set; }
        public bool IsFinal { get; private set; }
        public List<NodeConnection> Connections { get; private set; }
        public Node(string name, bool isFinal = false) {
            this.Name = name;
            this.IsFinal = isFinal;
            this.Connections = new List<NodeConnection>();
        }
        public void AddConnection(NodeConnection conn) {
            this.Connections.Add(conn);
        }

        public void MakeNodeFinal() {
            this.IsFinal = true;
        }

        override public string ToString() {
            string circle = this.IsFinal ? "doublecircle" : "circle";
            return $"\"{this.Name}\"[shape = {circle}]\n";
        }

        private List<NodeConnection> SortConnectionsByRules(string nextStep, PDAStack stack=null) {
            stack = stack ?? new PDAStack();
            List <NodeConnection> rule1 = new List<NodeConnection>();
            List < NodeConnection > rule3 = new List<NodeConnection>();
            List <NodeConnection> rule2 = new List<NodeConnection>();
            List < NodeConnection > rule4 = new List<NodeConnection>();
            if (stack.Count > 0) {
                string wantedStackIn = stack.PeekStack();
                rule1 = this.Connections.Where(c => c.StackIn == wantedStackIn && c.Value == nextStep).ToList();
                rule3 = this.Connections.Where(c => c.StackIn == wantedStackIn && c.Value == "_").ToList();
            }
            rule2 = this.Connections.Where(c => c.StackIn == "_" && c.Value == nextStep).ToList();
            rule4 = this.Connections.Where(c => c.StackIn == "_" && c.Value == "_").ToList();

            return rule1.Concat(rule2).Concat(rule3).Concat(rule4).ToList();
        }


        public bool IsStringPossible(
            string str, 
            int index,
            bool isPDA = false,
            bool hasBegun = false,
            string startStr = "",
            PDAStack stack = null,
            List<NodeConnection> passedConnections = null
        ){
            stack = stack ?? new PDAStack();
            passedConnections = passedConnections ?? new List<NodeConnection>();
            if (index > str.Length || !str.Contains(startStr)) {
                return false;
            }
            if(hasBegun) { 
                if (startStr == str && this.IsFinal && stack.Count == 0) {
                    return true;
                }
            }

            string symbol = index >= str.Length ? "_" : str[index] + "";
            List<NodeConnection> sortedConnections = SortConnectionsByRules(symbol, stack);
            foreach (NodeConnection conn in sortedConnections) {
                if (passedConnections.Contains(conn) && !isPDA) { 
                    int startOfLoop = passedConnections.IndexOf(conn);
                    bool isEmptyLoop = !passedConnections.Skip(startOfLoop).ToList().Exists(el => el.Value != "_");
                    if (isEmptyLoop) {
                        continue;
                    }
                }
                PDAStack newStack = stack.CloneStack();
                newStack.PopStack(conn.StackIn);
                newStack.PushStack(conn.StackOut);
                List<NodeConnection> currConns = passedConnections.ToList();
                currConns.Add(conn);
                if ("_" == conn.Value) {
                    bool isStrPos = conn.EndNode.IsStringPossible(str, index, isPDA, true, startStr + conn.GetVal(), newStack, currConns);
                    if (isStrPos) {
                        return true;
                    }
                } else if (symbol == conn.GetVal()) {
                    bool isStrPos = conn.EndNode.IsStringPossible(str, index + 1, isPDA, true, startStr + conn.GetVal(), newStack, currConns);
                    if (isStrPos) {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<string> GetAllWords(List<string> allWords, List<NodeConnection> enteredConnections = null) {
            // exit conditions
            if(allWords == null) {
                return null;
            }
            if (this.Connections.Count == 0) {
                if (this.IsFinal) {
                    allWords.Add("");
                }
                return allWords;
            }
            // copy objects
            enteredConnections = enteredConnections ?? new List<NodeConnection>();
            enteredConnections = enteredConnections.ToList();
            allWords = allWords.ToList();
            
            // start looping
            List<string> originalRange = allWords.ToList();
            foreach (var conn in this.Connections) {
                if (enteredConnections.Contains(conn)) {
                    int startOfLoop = enteredConnections.IndexOf(conn);
                    bool isEmptyLoop = !enteredConnections.Skip(startOfLoop).ToList().Exists(el => el.Value != "_");
                    if (isEmptyLoop) {
                        continue;
                    }
                    if (this.CanNodeReachFinalState()) {
                        return null;
                    } 
                    continue;
                }
                enteredConnections.Add(conn);

                List<string> newWords = conn.EndNode.GetAllWords(originalRange, enteredConnections);
                if (newWords == null) {
                    return null;
                }
                newWords = newWords.Select(val => conn.GetVal() + val).ToList();
                allWords.AddRange(newWords);
            }

            if (this.IsFinal) {
                allWords.Add("");
            }
            return allWords;
        }
        private bool CanNodeReachFinalState(List<NodeConnection> allTransitionsPassed = null) {
            allTransitionsPassed = allTransitionsPassed ?? new List<NodeConnection>();
            if (this.IsFinal) {
                return true;
            }
            bool endVal = false;
            foreach (var conn in Connections) {
                if (!allTransitionsPassed.Contains(conn)) {
                    allTransitionsPassed.Add(conn);
                    endVal = conn.EndNode.CanNodeReachFinalState(allTransitionsPassed);
                    if (endVal) {
                        return true;
                    }
                }
            }
            return endVal;
        }
        public List<Node> GetAllEndNodesConnectedToThisNode(string val) {
            return this.Connections.Where(c => c.Value == val).Select(c => c.EndNode).ToList();
        }
    }
}
