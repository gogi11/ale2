﻿using ALE2.Nodes;
using ALE2.Readers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ALE2{
    public partial class MainForm : Form{
        private Automaton automaton;
        private bool showTablesWhenConverting; 
        public MainForm(){
            InitializeComponent();
            SetNFAAutomaton("*(|(a,b))", "new_automaton.txt");
            this.showTablesWhenConverting = cbTableInfo.Checked;
        }

        private void CheckWord(string word, string expected = "-") {
            bool result = this.automaton.IsStringPossible(word);
            this.dgWords.Rows.Add(word, expected, result);
            this.dgWords.ClearSelection();
            if (expected == "-") {
                return;
            } else if(expected == result + "") {
                this.dgWords.Rows[dgWords.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Green;
            }else {
                this.dgWords.Rows[dgWords.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;
            }
        }

        private void ResetStuffBetweenAutomata(string formula) {
            this.pbGraph.ImageLocation = GenerateGraph.CreateImageAndReturnName(this.automaton);
            tbFormula.Text = formula;
            this.dgWords.Rows.Clear();
            lbAllPossibleWords.Items.Clear();
            lbAllPossibleWords.Visible = false;
            this.lbPDA.Text = this.automaton.IsPDA()? "True" : "False";
            DisplayAutomatonExtraInfo(this.automaton);
            Console.WriteLine(this.automaton.ToString());
        }

        private void SetNewAutomaton(string txt) {
            try { 
                this.automaton = AutomatonReader.ReadText(txt);
                ResetStuffBetweenAutomata(txt);
            } catch(Exception e) {
                MessageBox.Show(e.Message);
            }
        }
        private void SetNFAAutomaton(string formula, string fileName) {
            this.automaton = NFAReader.FindAutomaton(formula, fileName);
            ResetStuffBetweenAutomata(this.automaton.ToString());
        }
        private void ConvertNFAToDFAAutomaton(Automaton original) {
            if (this.automaton.IsPDA()) {
                MessageBox.Show("You cannot convert a PDA to a DFA!");
                return;
            }
            if (this.showTablesWhenConverting) {
                (Automaton a, ConnectionsTable first, ConnectionsTable second) = 
                    NfaToDfaConverter.ConvertToDFAWithStepsBetween(this.automaton);
                this.automaton = a;
                TablesAdditionalInfo form = new TablesAdditionalInfo(first, second);
                form.Show();
            } else {
                this.automaton = NfaToDfaConverter.ConvertToDFA(original);
            }
            ResetStuffBetweenAutomata(this.automaton.ToString());
        }

        private void pbGraph_Click(object sender, EventArgs e) {
            PictureBox pb = ((PictureBox)sender);
            if (pb.ImageLocation != null && pb.ImageLocation != "") { 
                Process.Start(pb.ImageLocation);
            }
        }

        private void btnFile_Click(object sender, EventArgs e) {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (op.ShowDialog() == DialogResult.OK) {
                string allTheText = System.IO.File.ReadAllText(op.FileName);
                SetNewAutomaton(allTheText);
            }
        }

        private void btnParse_Click(object sender, EventArgs e) {
            SetNewAutomaton(tbFormula.Text);
        }

        private void btnSave_Click(object sender, EventArgs e) {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (sf.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(sf.FileName, tbFormula.Text);
            }
        }

        private void btnRegex_Click(object sender, EventArgs e) {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (sf.ShowDialog() == DialogResult.OK) {
                SetNFAAutomaton(tbNfa.Text, sf.FileName);
            }
        }

        private void btnConvertToDfa_Click(object sender, EventArgs e) {
            ConvertNFAToDFAAutomaton(this.automaton);
        }

        private void DisplayAutomatonExtraInfo(Automaton a) {
            var isDfaExpected = a.Config.TestIsDfa;
            var finiteExpected = a.Config.TestIsFinite;
            Dictionary<string, bool> tested_words = a.Config.TestWords;

            lbDFAReal.Text = "-";
            lbDFAExpected.Text = GetCorrectMessage(isDfaExpected);
            if (isDfaExpected != TristateEnum.DOESNT_EXIST) {
                lbDFAReal.Text = a.IsDFA() + "";
            }

            lbFiniteReal.Text = "-";
            lbFiniteExpected.Text = GetCorrectMessage(finiteExpected);
            if (finiteExpected != TristateEnum.DOESNT_EXIST) {
                List<string> allWords = a.GetAllWords();
                setWords(allWords);
            }

            if (tested_words != null) { 
                foreach(KeyValuePair<string, bool> word in tested_words) {
                    CheckWord(word.Key, word.Value + "");
                }
            }
        }

        private string GetCorrectMessage(TristateEnum val) {
            string txt = "";
            if (val == TristateEnum.DOESNT_EXIST) {
                txt = "-";
            } else if (val == TristateEnum.FALSE) {
                txt = "False";
            } else {
                txt = "True";
            }
            return txt;
        }

        private void btnCheckWord_Click(object sender, EventArgs e) {
            CheckWord(tbWord.Text);
        }

        private void cbTableInfo_CheckedChanged(object sender, EventArgs e) {
            this.showTablesWhenConverting = ((CheckBox)sender).Checked;
        }

        private void btnDFACheck_Click(object sender, EventArgs e) {
            lbDFAReal.Text = "False";
            if (this.automaton.IsDFA()) {
                lbDFAReal.Text = "True";
            }
        }

        private void btnFiniteCheck_Click(object sender, EventArgs e) {
            List<string> allWords = this.automaton.GetAllWords();
            setWords(allWords);
        }

        private void setWords(List<string> allWords) {
            lbAllPossibleWords.Items.Clear();
            lbAllPossibleWords.Visible = false;
            if (allWords == null) {
                lbFiniteReal.Text = "False";
            } else if (allWords.Count == 0) {
                lbFiniteReal.Text = "True";
                lbAllPossibleWords.Visible = true;
                lbAllPossibleWords.Items.Add("There are no words accepted by this automaton.");
            } else {
                lbFiniteReal.Text = "True";
                lbAllPossibleWords.Visible = true;
                foreach (string s in allWords) {
                    lbAllPossibleWords.Items.Add(s);
                }
            }
        }
    }
}
