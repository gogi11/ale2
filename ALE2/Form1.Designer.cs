﻿namespace ALE2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbGraph = new System.Windows.Forms.PictureBox();
            this.btnFile = new System.Windows.Forms.Button();
            this.tbFormula = new System.Windows.Forms.TextBox();
            this.dgWords = new System.Windows.Forms.DataGridView();
            this.Word = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Expected = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDFAExpected = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbFiniteExpected = new System.Windows.Forms.Label();
            this.lbAllPossibleWords = new System.Windows.Forms.ListBox();
            this.btnParse = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbNfa = new System.Windows.Forms.TextBox();
            this.btnRegex = new System.Windows.Forms.Button();
            this.btnConvertToDfa = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCheckWord = new System.Windows.Forms.Button();
            this.tbWord = new System.Windows.Forms.TextBox();
            this.lbFiniteReal = new System.Windows.Forms.Label();
            this.lbDFAReal = new System.Windows.Forms.Label();
            this.cbTableInfo = new System.Windows.Forms.CheckBox();
            this.btnDFACheck = new System.Windows.Forms.Button();
            this.btnFiniteCheck = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lbPDA = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgWords)).BeginInit();
            this.SuspendLayout();
            // 
            // pbGraph
            // 
            this.pbGraph.Location = new System.Drawing.Point(279, 60);
            this.pbGraph.Name = "pbGraph";
            this.pbGraph.Size = new System.Drawing.Size(576, 489);
            this.pbGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbGraph.TabIndex = 0;
            this.pbGraph.TabStop = false;
            this.pbGraph.Click += new System.EventHandler(this.pbGraph_Click);
            // 
            // btnFile
            // 
            this.btnFile.Location = new System.Drawing.Point(23, 22);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(240, 32);
            this.btnFile.TabIndex = 1;
            this.btnFile.Text = "Select File";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // tbFormula
            // 
            this.tbFormula.AcceptsReturn = true;
            this.tbFormula.Location = new System.Drawing.Point(23, 60);
            this.tbFormula.Multiline = true;
            this.tbFormula.Name = "tbFormula";
            this.tbFormula.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbFormula.Size = new System.Drawing.Size(240, 413);
            this.tbFormula.TabIndex = 2;
            // 
            // dgWords
            // 
            this.dgWords.AllowUserToAddRows = false;
            this.dgWords.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgWords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgWords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Word,
            this.Expected,
            this.Actual});
            this.dgWords.Location = new System.Drawing.Point(880, 81);
            this.dgWords.Name = "dgWords";
            this.dgWords.RowHeadersVisible = false;
            this.dgWords.RowHeadersWidth = 51;
            this.dgWords.RowTemplate.Height = 24;
            this.dgWords.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgWords.Size = new System.Drawing.Size(304, 204);
            this.dgWords.TabIndex = 3;
            // 
            // Word
            // 
            this.Word.HeaderText = "Word";
            this.Word.MinimumWidth = 6;
            this.Word.Name = "Word";
            this.Word.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Expected
            // 
            this.Expected.HeaderText = "Expected";
            this.Expected.MinimumWidth = 6;
            this.Expected.Name = "Expected";
            this.Expected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Actual
            // 
            this.Actual.HeaderText = "Actual";
            this.Actual.MinimumWidth = 6;
            this.Actual.Name = "Actual";
            this.Actual.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(992, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Words";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(881, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "DFA:";
            // 
            // lbDFAExpected
            // 
            this.lbDFAExpected.AutoSize = true;
            this.lbDFAExpected.Location = new System.Drawing.Point(961, 328);
            this.lbDFAExpected.Name = "lbDFAExpected";
            this.lbDFAExpected.Size = new System.Drawing.Size(13, 17);
            this.lbDFAExpected.TabIndex = 6;
            this.lbDFAExpected.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(881, 365);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Is Finite?";
            // 
            // lbFiniteExpected
            // 
            this.lbFiniteExpected.AutoSize = true;
            this.lbFiniteExpected.Location = new System.Drawing.Point(961, 365);
            this.lbFiniteExpected.Name = "lbFiniteExpected";
            this.lbFiniteExpected.Size = new System.Drawing.Size(13, 17);
            this.lbFiniteExpected.TabIndex = 8;
            this.lbFiniteExpected.Text = "-";
            // 
            // lbAllPossibleWords
            // 
            this.lbAllPossibleWords.FormattingEnabled = true;
            this.lbAllPossibleWords.ItemHeight = 16;
            this.lbAllPossibleWords.Location = new System.Drawing.Point(880, 433);
            this.lbAllPossibleWords.Name = "lbAllPossibleWords";
            this.lbAllPossibleWords.Size = new System.Drawing.Size(304, 116);
            this.lbAllPossibleWords.TabIndex = 9;
            this.lbAllPossibleWords.Visible = false;
            // 
            // btnParse
            // 
            this.btnParse.Location = new System.Drawing.Point(23, 479);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(240, 32);
            this.btnParse.TabIndex = 11;
            this.btnParse.Text = "Parse Text";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.btnParse_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(23, 517);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(240, 32);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save To File";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbNfa
            // 
            this.tbNfa.Location = new System.Drawing.Point(279, 27);
            this.tbNfa.Name = "tbNfa";
            this.tbNfa.Size = new System.Drawing.Size(158, 22);
            this.tbNfa.TabIndex = 13;
            // 
            // btnRegex
            // 
            this.btnRegex.Location = new System.Drawing.Point(443, 22);
            this.btnRegex.Name = "btnRegex";
            this.btnRegex.Size = new System.Drawing.Size(124, 32);
            this.btnRegex.TabIndex = 14;
            this.btnRegex.Text = "Display NFA";
            this.btnRegex.UseVisualStyleBackColor = true;
            this.btnRegex.Click += new System.EventHandler(this.btnRegex_Click);
            // 
            // btnConvertToDfa
            // 
            this.btnConvertToDfa.Location = new System.Drawing.Point(573, 22);
            this.btnConvertToDfa.Name = "btnConvertToDfa";
            this.btnConvertToDfa.Size = new System.Drawing.Size(124, 32);
            this.btnConvertToDfa.TabIndex = 15;
            this.btnConvertToDfa.Text = "Convert to DFA";
            this.btnConvertToDfa.UseVisualStyleBackColor = true;
            this.btnConvertToDfa.Click += new System.EventHandler(this.btnConvertToDfa_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(961, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "Expected";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1060, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Real";
            // 
            // btnCheckWord
            // 
            this.btnCheckWord.Location = new System.Drawing.Point(1101, 48);
            this.btnCheckWord.Name = "btnCheckWord";
            this.btnCheckWord.Size = new System.Drawing.Size(83, 27);
            this.btnCheckWord.TabIndex = 20;
            this.btnCheckWord.Text = "Check";
            this.btnCheckWord.UseVisualStyleBackColor = true;
            this.btnCheckWord.Click += new System.EventHandler(this.btnCheckWord_Click);
            // 
            // tbWord
            // 
            this.tbWord.Location = new System.Drawing.Point(880, 50);
            this.tbWord.Name = "tbWord";
            this.tbWord.Size = new System.Drawing.Size(215, 22);
            this.tbWord.TabIndex = 19;
            // 
            // lbFiniteReal
            // 
            this.lbFiniteReal.AutoSize = true;
            this.lbFiniteReal.Location = new System.Drawing.Point(1060, 365);
            this.lbFiniteReal.Name = "lbFiniteReal";
            this.lbFiniteReal.Size = new System.Drawing.Size(13, 17);
            this.lbFiniteReal.TabIndex = 22;
            this.lbFiniteReal.Text = "-";
            // 
            // lbDFAReal
            // 
            this.lbDFAReal.AutoSize = true;
            this.lbDFAReal.Location = new System.Drawing.Point(1060, 328);
            this.lbDFAReal.Name = "lbDFAReal";
            this.lbDFAReal.Size = new System.Drawing.Size(13, 17);
            this.lbDFAReal.TabIndex = 21;
            this.lbDFAReal.Text = "-";
            // 
            // cbTableInfo
            // 
            this.cbTableInfo.AutoSize = true;
            this.cbTableInfo.Location = new System.Drawing.Point(703, 28);
            this.cbTableInfo.Name = "cbTableInfo";
            this.cbTableInfo.Size = new System.Drawing.Size(176, 21);
            this.cbTableInfo.TabIndex = 23;
            this.cbTableInfo.Text = "View Intermidiate Steps";
            this.cbTableInfo.UseVisualStyleBackColor = true;
            this.cbTableInfo.CheckedChanged += new System.EventHandler(this.cbTableInfo_CheckedChanged);
            // 
            // btnDFACheck
            // 
            this.btnDFACheck.Location = new System.Drawing.Point(1110, 322);
            this.btnDFACheck.Name = "btnDFACheck";
            this.btnDFACheck.Size = new System.Drawing.Size(74, 29);
            this.btnDFACheck.TabIndex = 24;
            this.btnDFACheck.Text = "check";
            this.btnDFACheck.UseVisualStyleBackColor = true;
            this.btnDFACheck.Click += new System.EventHandler(this.btnDFACheck_Click);
            // 
            // btnFiniteCheck
            // 
            this.btnFiniteCheck.Location = new System.Drawing.Point(1110, 357);
            this.btnFiniteCheck.Name = "btnFiniteCheck";
            this.btnFiniteCheck.Size = new System.Drawing.Size(74, 29);
            this.btnFiniteCheck.TabIndex = 25;
            this.btnFiniteCheck.Text = "check";
            this.btnFiniteCheck.UseVisualStyleBackColor = true;
            this.btnFiniteCheck.Click += new System.EventHandler(this.btnFiniteCheck_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(881, 401);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 17);
            this.label6.TabIndex = 26;
            this.label6.Text = "PDA";
            // 
            // lbPDA
            // 
            this.lbPDA.AutoSize = true;
            this.lbPDA.Location = new System.Drawing.Point(1060, 401);
            this.lbPDA.Name = "lbPDA";
            this.lbPDA.Size = new System.Drawing.Size(13, 17);
            this.lbPDA.TabIndex = 27;
            this.lbPDA.Text = "-";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 572);
            this.Controls.Add(this.lbPDA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnFiniteCheck);
            this.Controls.Add(this.btnDFACheck);
            this.Controls.Add(this.cbTableInfo);
            this.Controls.Add(this.lbFiniteReal);
            this.Controls.Add(this.lbDFAReal);
            this.Controls.Add(this.btnCheckWord);
            this.Controls.Add(this.tbWord);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnConvertToDfa);
            this.Controls.Add(this.btnRegex);
            this.Controls.Add(this.tbNfa);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnParse);
            this.Controls.Add(this.lbAllPossibleWords);
            this.Controls.Add(this.lbFiniteExpected);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbDFAExpected);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgWords);
            this.Controls.Add(this.tbFormula);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.pbGraph);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "Automaton Solving App";
            ((System.ComponentModel.ISupportInitialize)(this.pbGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgWords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbGraph;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.TextBox tbFormula;
        private System.Windows.Forms.DataGridView dgWords;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbDFAExpected;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbFiniteExpected;
        private System.Windows.Forms.ListBox lbAllPossibleWords;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbNfa;
        private System.Windows.Forms.Button btnRegex;
        private System.Windows.Forms.Button btnConvertToDfa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCheckWord;
        private System.Windows.Forms.TextBox tbWord;
        private System.Windows.Forms.DataGridViewTextBoxColumn Word;
        private System.Windows.Forms.DataGridViewTextBoxColumn Expected;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actual;
        private System.Windows.Forms.Label lbFiniteReal;
        private System.Windows.Forms.Label lbDFAReal;
        private System.Windows.Forms.CheckBox cbTableInfo;
        private System.Windows.Forms.Button btnDFACheck;
        private System.Windows.Forms.Button btnFiniteCheck;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbPDA;
    }
}

