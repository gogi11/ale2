﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2 {
    public class AutomatonConfig {
        public string RegularExpression { get; private set; }
        public Stack<string> Stack { get; private set; }
        public TristateEnum TestIsDfa { get; private set; }
        public TristateEnum TestIsFinite { get; private set; }
        public Dictionary<string, bool> TestWords { get; private set; }

        public AutomatonConfig(
            string regularExpression = "", 
            Stack<string> stack = null, 
            TristateEnum testIsDfa = TristateEnum.DOESNT_EXIST, 
            TristateEnum testIsFinite = TristateEnum.DOESNT_EXIST, 
            Dictionary<string, bool> testWords = null
        ) {
            this.RegularExpression = regularExpression;
            this.Stack = stack;
            this.TestIsDfa = testIsDfa;
            this.TestIsFinite = testIsFinite;
            this.TestWords = testWords;
        }

    }
}
