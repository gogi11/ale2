﻿using ALE2.Interfaces;
using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2 {
    public class Automaton: IAutomaton {
        public Node StartNode { get; private set; }
        public List<Node> FinalNodes { get; private set; }
        public List<Node> AllNodes { get; private set; }
        public List<NodeConnection> AllConnections { get; private set; }
        public List<string> Alphabet { get; private set; }
        public AutomatonConfig Config { get; private set; }

        public Automaton(List<Node> allNodes, List<Node> finalNodes, 
            List<NodeConnection> allConnections, List<string> alphabet,
            AutomatonConfig config){
            this.Config = config;
            this.AllNodes = allNodes;
            this.FinalNodes = finalNodes;
            this.StartNode = allNodes[0];
            this.AllConnections = allConnections;
            this.Alphabet = alphabet;
        }

        public bool IsDFA() {
            if (this.IsPDA()) {
                return false;
            }
            foreach(Node node in this.AllNodes) {
                List<NodeConnection> connections = node.Connections;
                string alph = String.Join("", this.Alphabet);
                if (connections.Count != alph.Length) {
                    return false;
                }
                foreach (NodeConnection conn in connections) {
                    if (conn.Value == "_") {
                        return false;
                    }
                    if (!alph.Contains(conn.GetVal())) {
                        return false;
                    }
                    alph.Replace(conn.GetVal(), "");
                }
            }
            return true;
        }

        public bool IsStringPossible(string str) {
            foreach(char s in str) {
                if(!Alphabet.Contains(s+"")) {
                    return false;
                }
            }
            return StartNode.IsStringPossible(str, 0, this.IsPDA());
        }

        public List<string> GetAllWords() {
            if (this.IsPDA()) {
                return null;
            }
            var allWords = StartNode.GetAllWords(new List<string>());
            if(allWords == null) {
                return allWords;
            }
            allWords = allWords.Distinct().ToList();
            if (allWords.Contains("") && !this.IsStringPossible("")) {
                allWords.Remove("");
            }
            return allWords;
        }

        public override string ToString() {
            string str = "";
            if (this.Config.RegularExpression != "") {
                str += "# Regular Expression: " + this.Config.RegularExpression + "\r\n";
            }
            str += "alphabet: " + String.Join("", this.Alphabet.ToArray()) + "\r\n";
            if(this.Config.Stack != null && this.Config.Stack.Count > 0) {
                str += "stack: " + String.Join("", this.Config.Stack.ToArray()) + "\r\n";
            }
            str += "states: " + String.Join(",", this.AllNodes.Select(n => n.Name)) + "\r\n";
            str += "final: " + String.Join(",", this.FinalNodes.Select(n => n.Name)) + "\r\n";
            str += "transitions:\r\n";
            foreach (NodeConnection c in this.AllConnections) {
                str += $"{c.StartNode.Name},{c.GetLabel()} --> {c.EndNode.Name}\r\n";
            }
            str += "end.";
            return str;
        }

        public void SaveToFile(string fileName) {
            System.IO.File.WriteAllText(fileName, this.ToString());
        }

        public bool IsPDA() {
            return this.Config.Stack != null && this.Config.Stack.Count > 0;
        }
    }
}
