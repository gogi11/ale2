﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Interfaces {
    public interface IPDAStack {
        string PeekStack();
        PDAStack CloneStack();
        void PopStack(string symbol);
        void PushStack(string symbol);
    }
}
