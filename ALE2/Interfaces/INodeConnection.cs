﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Interfaces {
    public interface INodeConnection {
        string GetVal();
        string GetLabel();
    }
}
