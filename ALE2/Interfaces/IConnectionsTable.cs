﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Interfaces {
    public interface IConnectionsTable {
        ConnectionsCell GetCell(Node startNode, string letterFromAlphabet);
    }
    public interface IConnectionsRow {
        ConnectionsCell GetCell(int index);
    }
    public interface IConnectionsCell {
        void AddNode(Node n);
        void AddNodesFromCell(ConnectionsCell cell);
        bool IsCellFinal();
    }
}
