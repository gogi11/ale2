﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Interfaces {
    public interface IAutomaton {
        bool IsDFA();
        bool IsStringPossible(string str);
        List<string> GetAllWords();
        void SaveToFile(string fileName);
        bool IsPDA();
    }
}
