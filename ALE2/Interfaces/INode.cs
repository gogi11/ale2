﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Interfaces {
    public interface INode {
        void AddConnection(NodeConnection conn);
        void MakeNodeFinal();
        bool IsStringPossible(string str, int index, bool isPDA, bool hasBegun, string startStr, 
            PDAStack stack, List<NodeConnection> passedConnections);
        List<string> GetAllWords(List<string> allWords, List<NodeConnection> enteredConnections);
        List<Node> GetAllEndNodesConnectedToThisNode(string val);
    }
}
