﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ALE2 {
    public partial class TablesAdditionalInfo : Form {
        public TablesAdditionalInfo(ConnectionsTable first, ConnectionsTable second) {
            InitializeComponent();
            AddColumnsToDataGrid(first, dgFirst);
            AddColumnsToDataGrid(second, dgSecond);
            FillInDataGrid(first, dgFirst);
            FillInDataGrid(second, dgSecond);
        }
        private void AddColumnsToDataGrid(ConnectionsTable table, DataGridView dgView) {
            dgView.Columns.Add("Start\\Letter", "Start\\Letter");
            foreach (string letter in table.Alphabet) {
                dgView.Columns.Add(letter.Replace("_", "ε"), letter.Replace("_", "ε"));
            }
        }
        private void FillInDataGrid(ConnectionsTable table, DataGridView dgView) {
            foreach (ConnectionsRow row in table.Rows) {
                List<string> dgRow = new List<string>();
                dgRow.Add((row.StartingCell + "").Replace("_", "ε"));
                foreach (ConnectionsCell cell in row.Cells) {
                    dgRow.Add((cell + "").Replace("_", "ε"));
                }
                dgView.Rows.Add(dgRow.ToArray());
            }
        }
    }
}
