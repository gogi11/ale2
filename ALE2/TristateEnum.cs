﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2 {
    public enum TristateEnum {
        FALSE=0, TRUE=1, DOESNT_EXIST=-1
    }
    public static class TristateHandler {
        public static bool TristateToBool(TristateEnum val) {
            if (val == TristateEnum.DOESNT_EXIST) {
                throw new Exception("The object doesn't exist!");
            }
            return val == TristateEnum.TRUE;
        }
        public static TristateEnum BoolToTristate(bool val) {
            if (val) {
                return TristateEnum.TRUE;
            }
            return TristateEnum.FALSE;
        }
        public static TristateEnum TriAndTriOperator(TristateEnum first, TristateEnum second) {
            if(first == TristateEnum.DOESNT_EXIST || second == TristateEnum.DOESNT_EXIST) {
                return TristateEnum.DOESNT_EXIST;
            }
            bool a = TristateToBool(first);
            bool b = TristateToBool(second);
            return BoolToTristate(a && b);
        }
        public static TristateEnum BoolAndTriOperator(bool first, TristateEnum second) {
            TristateEnum a = BoolToTristate(first);
            return TriAndTriOperator(a, second);
        }
    }
}
