﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.NfaDfaConvertor {
    public static class EClosureTableToDfaTableCoverter {
        public static ConnectionsTable MakeTable(Automaton a, ConnectionsTable original) {
            List<ConnectionsRow> newrows = GetAllNewRows(original, a.StartNode);
            return new ConnectionsTable(newrows, a.Alphabet);
        }

        private static List<ConnectionsRow> GetAllNewRows(ConnectionsTable table, Node startNode) {
            List<ConnectionsRow> addedRows = new List<ConnectionsRow>() { };
            List<ConnectionsCell> startingCellsToAdd = new List<ConnectionsCell>() {
                new ConnectionsCell(
                    new List<Node>() { startNode }
                )
            };
            while (startingCellsToAdd.Count > 0) {
                var cell = startingCellsToAdd[0];
                ConnectionsRow newRow = GetNewRow(table, cell.Nodes);
                if (!addedRows.Exists(row => row.StartingCell.Equals(newRow.StartingCell))) {
                    addedRows.Add(newRow);
                    foreach (var newCell in newRow.Cells) {
                        if (!startingCellsToAdd.Contains(newCell) && !addedRows.Exists(row => row.StartingCell.Equals(newCell))) {
                            startingCellsToAdd.Add(newCell);
                        }
                    }
                }
                startingCellsToAdd.RemoveAt(0);
            }
            return addedRows;
        }

        private static ConnectionsRow GetNewRow(ConnectionsTable table, List<Node> fromNodes) {
            ConnectionsCell c = GetEClosure(table, fromNodes);
            List<ConnectionsCell> allCellsPerLetter = new List<ConnectionsCell>();
            foreach (string letter in table.Alphabet) {
                if (letter == "_") { continue; }
                ConnectionsCell temp = new ConnectionsCell(new List<Node>());
                foreach (Node n in c.Nodes) {
                    temp.AddNodesFromCell(GetEClosure(table, table.GetCell(n, letter).Nodes));
                }
                allCellsPerLetter.Add(temp);
            }
            return new ConnectionsRow(c.Nodes, allCellsPerLetter);
        }

        private static ConnectionsCell GetEClosure(ConnectionsTable table, List<Node> fromNodes) {
            fromNodes = fromNodes.ToList();
            int index = 0;
            List<Node> goneThrough = new List<Node>();
            ConnectionsCell c = new ConnectionsCell(fromNodes);
            while (!IsSameList(goneThrough, c.Nodes)) {
                c.AddNodesFromCell(table.GetCell(c.Nodes[index], "_"));
                goneThrough.Add(c.Nodes[index]);
                index++;
            }
            return c;
        }

        private static bool IsSameList<T>(List<T> list1, List<T> list2) {
            var firstNotSecond = list1.Except(list2).ToList();
            var secondNotFirst = list2.Except(list1).ToList();
            return !firstNotSecond.Any() && !secondNotFirst.Any();
        }
    }
}
