﻿using ALE2.Interfaces;
using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2 {
    public class ConnectionsTable: IConnectionsTable {
        public List<ConnectionsRow> Rows { get; private set; }
        public List<string> Alphabet { get; private set; }
        public ConnectionsTable(Automaton a) {
            this.Alphabet = a.Alphabet.ToList();
            this.Alphabet.Add("_");
            this.Rows = new List<ConnectionsRow>();
            foreach (Node n in a.AllNodes) {
                List<ConnectionsCell> cells = new List<ConnectionsCell>();
                foreach (string letter in this.Alphabet) {
                    cells.Add(new ConnectionsCell(n.GetAllEndNodesConnectedToThisNode(letter)));
                }
                this.Rows.Add(new ConnectionsRow(n, cells));
            }
        }
        public ConnectionsTable(List<ConnectionsRow> rows, List<string> alphabet) {
            this.Rows = rows;
            this.Alphabet = alphabet;
        }

        public override string ToString() {
            string val = "";
            for (int i = 0; i < Alphabet.Count; i++) {
                val += "\t" + Alphabet[i];
            }
            val += "\n";
            foreach (ConnectionsRow r in this.Rows) {
                val += r.ToString() + "\n";
            }
            return val;
        }

        public ConnectionsCell GetCell(Node startNode, string letterFromAlphabet) {
            foreach(ConnectionsRow row in this.Rows) { 
                if(row.StartingCell.ToString() == startNode.Name) { 
                    return row.GetCell(this.Alphabet.IndexOf(letterFromAlphabet));
                }
            }
            return null;
        }
    }
    public class ConnectionsRow: IConnectionsRow {
        public List<ConnectionsCell> Cells { get; private set; }
        public ConnectionsCell StartingCell { get; private set; }
        public ConnectionsRow(Node start, List<ConnectionsCell> cells) {
            this.StartingCell = new ConnectionsCell(new List<Node>() { start });
            this.Cells = cells;
        }
        public ConnectionsRow(List<Node> start, List<ConnectionsCell> cells) {
            this.StartingCell = new ConnectionsCell(start);
            this.Cells = cells;
        }
        public override string ToString() {
            return this.StartingCell + "\t" + String.Join("\t", this.Cells);
        }
        public ConnectionsCell GetCell(int index) {
            return this.Cells[index];
        }
    }
    public class ConnectionsCell: IConnectionsCell {
        public List<Node> Nodes { get; private set; }

        public ConnectionsCell(List<Node> nodes) {
            this.Nodes = nodes;
        }

        public void AddNode(Node n) {
            if (!this.Nodes.Contains(n)) { 
                this.Nodes.Add(n);
            }
        }

        public void AddNodesFromCell(ConnectionsCell cell) {
            foreach(Node n in cell.Nodes) {
                this.AddNode(n);
            }
        }

        public override string ToString() {
            this.Nodes.Sort((a, b) => a.Name.CompareTo(b.Name));
            string val = String.Join(",", this.Nodes.Select(n => n.Name));
            return val == "" ? "_" : val;
        }
        public override bool Equals(object obj) {
            return obj is ConnectionsCell && ((ConnectionsCell)obj).ToString() == this.ToString();
        }
        public override int GetHashCode() {
            return base.GetHashCode();
        }
        public bool IsCellFinal() {
            foreach(var n in this.Nodes) {
                if (n.IsFinal) {
                    return true;
                }
            }
            return false;
        }
    }
}
