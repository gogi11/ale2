﻿using ALE2.NfaDfaConvertor;
using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2 {
    public static class NfaToDfaConverter {
        public static Automaton ConvertToDFA(Automaton a) {
            if (a.IsPDA()) {
                return null;
            }
            ConnectionsTable table1 = new ConnectionsTable(a);
            var table2 = EClosureTableToDfaTableCoverter.MakeTable(a, table1);
            return MakeAutomatonFromTable(table2, a);
        }

        public static (Automaton, ConnectionsTable, ConnectionsTable) ConvertToDFAWithStepsBetween(Automaton a) {
            if (a.IsPDA()) {
                return (null, null, null);
            }
            ConnectionsTable table1 = new ConnectionsTable(a);
            var table2 = EClosureTableToDfaTableCoverter.MakeTable(a, table1);
            return (MakeAutomatonFromTable(table2, a), table1, table2);
        }

        private static Node GetNodeWithName(List<Node> nodes, string name) {
            foreach(Node n in nodes) {
                if (n.Name == name) {
                    return n;
                }
            }
            return null;
        }
        private static Node GetOrCreateNodeFromListsAndTableCell(
            Dictionary<string, string> namingDict,
            ConnectionsCell cellToAdd,
            List<Node> allNodes,
            List<Node> finalNodes
        ) {
            if (!namingDict.ContainsKey(cellToAdd.ToString())) {
                return null;
            }
            string startingRowName = namingDict[cellToAdd.ToString()];
            bool isFinal = cellToAdd.IsCellFinal();
            Node fromList = GetNodeWithName(allNodes, startingRowName);
            if (fromList == null) {
                Node newNode = new Node(startingRowName, isFinal);
                allNodes.Add(newNode);
                if (isFinal) {
                    finalNodes.Add(newNode);
                }
                return newNode;
            }
            return fromList;
        }
        private static Automaton MakeAutomatonFromTable(ConnectionsTable table, Automaton original) {
            int id = 0;
            string symbol = "S";
            Dictionary<string, string> naming = new Dictionary<string, string>();
            List<Node> allNodes = new List<Node>();
            List<Node> finalNodes = new List<Node>();
            List<NodeConnection> allConnections = new List<NodeConnection>();
            foreach (var row in table.Rows) {
                naming.Add(row.StartingCell.ToString(), symbol+id);
                id++;
            }
            foreach (var row in table.Rows) {
                Node start = GetOrCreateNodeFromListsAndTableCell(naming, row.StartingCell, allNodes, finalNodes);
                for (int i= 0; i<row.Cells.Count; i++) {
                    Node end = GetOrCreateNodeFromListsAndTableCell(naming, row.Cells[i], allNodes, finalNodes);
                    if (end != null) {
                        NodeConnection conn = new NodeConnection(start, end, original.Alphabet[i]);
                        allConnections.Add(conn);
                        start.AddConnection(conn);
                    }
                }
            }
            Automaton a = new Automaton(allNodes, finalNodes, allConnections, original.Alphabet, new AutomatonConfig());
            return a;
        }
    }
}
