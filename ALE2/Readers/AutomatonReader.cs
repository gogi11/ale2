﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ALE2.Readers
{
    static public class AutomatonReader {
        private static Node GetStateWithName(List<Node> states, string name) {
            foreach(Node n in states) {
                if(n.Name == name) {
                    return n;
                }
            }
            return null;
        }

        private static TristateEnum GetVal(string line, string errormsg) {
            if (line.ToLower() == "y") {
                return TristateEnum.TRUE;
            } else if (line.ToLower() == "n") {
                return TristateEnum.FALSE;
            } else {
                throw new Exception(errormsg);
            }
        }

        public static Automaton ReadFile(string fileName) {
            string allTheText = System.IO.File.ReadAllText(fileName);
            return ReadText(allTheText);
        }

        public static Automaton ReadText(string theWholeText) {
            string allPossibleLetters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+-*/()";

            theWholeText = theWholeText.Replace(" ", "").Replace("\r", "");
            string[] lines = theWholeText.Split('\n');
            lines = lines.Where(el => el.Length > 0).ToArray();
            string line;
            List<Node> states = new List<Node>();
            List<Node> finalNodes = new List<Node>();
            List<NodeConnection> connections = new List<NodeConnection>();
            List<string> alphabet = new List<string>() { "_" };
            List<string> stack = new List<string>() { "_" };

            TristateEnum isDfa = TristateEnum.DOESNT_EXIST;
            TristateEnum isFinite = TristateEnum.DOESNT_EXIST;
            Dictionary<string, bool> wordsToCheck = null;
            for(int i=0; i< lines.Length; i++) {
                line = lines[i];
                if (line.Length > 0) {
                    if (line[0] == '#') {// it's a comment
                        continue;
                    } else if (line.IndexOf("alphabet:") == 0) {// it's the alphabet
                        line = line.Replace("alphabet:", "");
                        var strangeLetters = line.Where(c => !allPossibleLetters.Contains(c)).ToList();
                        if (strangeLetters != null && strangeLetters.Count() > 0) {
                            string errorMsg = "";
                            for(int m=0; m< strangeLetters.Count(); m++) {
                                errorMsg += "Incorrect input! You have entered a letter in the alphabet which " +
                                    "is not accepted!\nLetter: " + strangeLetters[m] + "\n\n";
                            }
                            throw new Exception(errorMsg);
                        }
                        alphabet = alphabet.Concat(line.Select(c => c.ToString())).ToList();
                        continue;
                    } else if (line.IndexOf("states:") == 0) {// it's the states
                        line = line.Replace("states:", "");
                        string[] allStates = line.Split(',');
                        foreach (string state in allStates) {
                            if (GetStateWithName(states, state) == null) {
                                states.Add(new Node(state));
                            } else {
                                throw new Exception("Incorrect input! You can only have 1 state with that name!\nName: " + state);
                            }
                        }
                        continue;
                    } else if (line.IndexOf("stack:") == 0) {// it's the stack
                        line = line.Replace("stack:", "");
                        var strangeLetters = line.Where(c => !allPossibleLetters.Contains(c)).ToList();
                        if (strangeLetters != null && strangeLetters.Count() > 0) {
                            string errorMsg = "";
                            for (int m = 0; m < strangeLetters.Count(); m++) {
                                errorMsg += "Incorrect input! You have entered a letter in the stack which " +
                                    "is not accepted!\nStack Letter: " + strangeLetters[m] + "\n\n";
                            }
                            throw new Exception(errorMsg);
                        }
                        stack = stack.Concat(line.Select(c => c.ToString())).ToList();
                        continue;
                    } else if (line.IndexOf("final:") == 0) {// it's the final
                        line = line.Replace("final:", "");
                        string[] finalStates = line.Split(',');
                        foreach (string state in finalStates) {
                            Node finalNode = GetStateWithName(states, state);
                            if (finalNode != null) {
                                finalNode.MakeNodeFinal();
                                finalNodes.Add(finalNode);
                            } else {
                                throw new Exception("Incorrect input! You haven't specified this state!\nName: " + state);
                            }
                        }
                        continue;
                    } else if (line.IndexOf("transitions:") == 0) {
                        for (++i; i < lines.Length; i++) {
                            line = lines[i];
                            if(line.IndexOf("end.") == 0) {
                                break;
                            }
                            if (line[0] == '#') {// it's a comment
                                continue;
                            }
                            string node1name = line.Split(',')[0];
                            line = line.Split(new string[] {","}, 2, StringSplitOptions.None)[1];
                            int index = line.IndexOf("-->");
                            string connectionString = line.Substring(0, index);
                            string stackIn = "_";
                            string stackOut = "_";
                            if (connectionString.Length>1) { // so if it's line a[x,y]
                                stackIn = connectionString[2]+"";
                                stackOut = connectionString[4]+"";
                                connectionString = connectionString[0] + "";
                                if (stack.IndexOf(stackIn) < 0) {
                                    throw new Exception("Incorrect input! You haven't specified this stack letter! " + stackIn);
                                }
                                if (stack.IndexOf(stackOut) < 0) {
                                    throw new Exception("Incorrect input! You haven't specified this stack letter! " + stackOut);
                                }
                            }
                            if (alphabet.IndexOf(connectionString)<0) {
                                throw new Exception("Incorrect input! You haven't specified this letter in the alphabet! "+connectionString);
                            }
                            string node2name = line.Substring(index+3, line.Length-index-3);
                            Node start = GetStateWithName(states, node1name);
                            if(start == null) {
                                throw new Exception("Incorrect input! You haven't specified this state!\nName: " + node1name);
                            }
                            Node end = GetStateWithName(states, node2name);
                            if (end == null) {
                                throw new Exception("Incorrect input! You haven't specified this state!\nName: " + node2name);
                            }
                            NodeConnection newConn = new NodeConnection(start, end, connectionString, stackIn, stackOut);
                            connections.Add(newConn);
                            start.AddConnection(newConn);
                        }
                    } else if (line.IndexOf("dfa:") == 0) {// it's the stack
                        line = line.Replace("dfa:", "");
                        string errorMsg = "Incorrect input! You need to specify the \"dfa\" as  either \"y\" or \"n\". You sepcified " + line;
                        isDfa = GetVal(line, errorMsg);
                        continue;
                    } else if (line.IndexOf("finite:") == 0) {// it's the stack
                        line = line.Replace("finite:", "");
                        string errorMsg = "Incorrect input! You need to specify the \"finite\" as  either \"y\" or \"n\". You sepcified " + line;
                        isFinite = GetVal(line, errorMsg);
                        continue;
                    } else if (line.IndexOf("words:") == 0) {
                        wordsToCheck = new Dictionary<string, bool>();
                        for (++i;  i < lines.Length; i++) {
                            line = lines[i];
                            if (line.IndexOf("end.") == 0) {
                                break;
                            }
                            if (line[0] == '#') {// it's a comment
                                continue;
                            }
                            string word = line.Split(',')[0];
                            string wordval = line.Split(',')[1];
                            if (wordsToCheck.ContainsKey(word)) {
                                throw new Exception("Please specify each word only once!");
                            }
                            string errorMsg = "Incorrect input! You need to specify the word followed by either \"y\" or \"n\"! You specified " + wordval;
                            bool val = TristateHandler.TristateToBool(GetVal(wordval, errorMsg));
                            wordsToCheck.Add(word, val);
                        }
                    }
                }
            }
            alphabet.Remove("_");
            stack.Remove("_");
            stack.Reverse();
            AutomatonConfig config = new AutomatonConfig("", new Stack<string>(stack), isDfa, isFinite, wordsToCheck);
            return new Automaton(states, finalNodes, connections, alphabet, config);
        }
    }
}
