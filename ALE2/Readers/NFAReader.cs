﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE2.Readers {
    public static class NFAReader {

        private static int nodeID = 0;
        private static List<Node> allNodes;
        private static List<NodeConnection> allConnections;
        private static List<string> alphabet;

        private static Node NewNode() {
            Node n = new Node("n" + nodeID);
            nodeID++;
            allNodes.Add(n);
            return n;
        }
        private static void AddConnection(Node start, Node end, string val="_") {
            NodeConnection c = new NodeConnection(start, end, val);
            alphabet.Add(val);
            start.AddConnection(c);
            allConnections.Add(c);
        }
        private static void FixAlphabet() {
            alphabet = alphabet.Distinct().ToList();
            alphabet.RemoveAll(letter => letter == "_");
        }
        private static string[] GetTwoResultsSeperatedByComma(string re) {
            string[] ret = new string[2];
            int nrOfBrackets = 1;
            for (int i = 2; i < re.Length; i++) {
                if (re[i] == '(') {
                    nrOfBrackets++;
                } else if (re[i] == ')') {
                    nrOfBrackets--;
                }

                if (nrOfBrackets == 1 && re[i] == ',') {
                    ret[0] = re.Substring(2, i-2); 
                    ret[1] = re.Substring(i+1, re.Length-i-2);
                    break;
                }
            }
            return ret;
        }
        public static Automaton FindAutomaton(string re, string newFileName) {
            nodeID = 0;
            allNodes = new List<Node>();
            allConnections = new List<NodeConnection>();
            alphabet = new List<string>();
            Node end = ReadRegularExpression(re)[1];
            end.MakeNodeFinal();
            FixAlphabet();
            AutomatonConfig config = new AutomatonConfig(re);
            Automaton a = new Automaton(allNodes, new List<Node>() { end }, allConnections, alphabet, config);
            a.SaveToFile(newFileName);
            return a;
        }
        private static Node[] ReadRegularExpression(string re) {
            Node start = NewNode();
            Node end = NewNode();
            if (re[0] == '|') {
                string[] twoParts = GetTwoResultsSeperatedByComma(re);
                Node[] n1 = ReadRegularExpression(twoParts[0]);
                Node[] n2 = ReadRegularExpression(twoParts[1]);

                AddConnection(start, n1[0]);
                AddConnection(start, n2[0]);

                AddConnection(n1[1], end);
                AddConnection(n2[1], end);
            } else if (re[0] == '.') {
                string[] twoParts = GetTwoResultsSeperatedByComma(re);
                Node[] n1 = ReadRegularExpression(twoParts[0]);
                Node[] n2 = ReadRegularExpression(twoParts[1]);

                AddConnection(start, n1[0]);
                AddConnection(n1[1], n2[0]);
                AddConnection(n2[1], end);
            } else if (re[0] == '*') {
                re = re.Substring(2, re.Length - 3);
                Node[] n = ReadRegularExpression(re);

                AddConnection(start, n[0]);
                AddConnection(start, end);
                AddConnection(n[1], end);
                AddConnection(n[1], n[0]);
            } else if (re.Length == 1) {
                AddConnection(start, end, re[0] + "");
            } else {
                throw new Exception("This part of the expression is incorrect: "+re);
            }
            return new Node[] { start, end };
        }
    }
}
