﻿namespace ALE2 {
    partial class TablesAdditionalInfo {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgFirst = new System.Windows.Forms.DataGridView();
            this.dgSecond = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSecond)).BeginInit();
            this.SuspendLayout();
            // 
            // dgFirst
            // 
            this.dgFirst.AllowUserToAddRows = false;
            this.dgFirst.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFirst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFirst.Location = new System.Drawing.Point(29, 60);
            this.dgFirst.Name = "dgFirst";
            this.dgFirst.RowHeadersVisible = false;
            this.dgFirst.RowHeadersWidth = 51;
            this.dgFirst.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFirst.Size = new System.Drawing.Size(346, 349);
            this.dgFirst.TabIndex = 0;
            // 
            // dgSecond
            // 
            this.dgSecond.AllowUserToAddRows = false;
            this.dgSecond.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSecond.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSecond.Location = new System.Drawing.Point(433, 60);
            this.dgSecond.Name = "dgSecond";
            this.dgSecond.RowHeadersVisible = false;
            this.dgSecond.RowHeadersWidth = 51;
            this.dgSecond.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSecond.Size = new System.Drawing.Size(346, 349);
            this.dgSecond.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Original Table";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(555, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Final Table";
            // 
            // TablesAdditionalInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgSecond);
            this.Controls.Add(this.dgFirst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "TablesAdditionalInfo";
            this.Text = "NFA to DFA Conversion Additional Info";
            ((System.ComponentModel.ISupportInitialize)(this.dgFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSecond)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgFirst;
        private System.Windows.Forms.DataGridView dgSecond;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}