﻿using ALE2.Nodes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ALE2 {
    public static class GenerateGraph {
        private static void WriteDot(Automaton a, string filename) {
            string content = "digraph myAutomaton {\n rankdir=LR;\n \"\" [shape=none] \n";
            List<Node> allNodes = a.AllNodes;
            foreach (var node in allNodes) {
                content += node;
            }
            content += $"\"\" -> \"{allNodes[0].Name}\"\n";
            foreach (var conn in a.AllConnections) {
                content += conn;
            }
            content += "}";

            File.WriteAllText(filename, content);
        }

        public static string CreateImageAndReturnName(Automaton a) {
            WriteDot(a, "abc.dot");
            Process dot = new Process();
            dot.StartInfo.FileName = "dot.exe";
            dot.StartInfo.Arguments = "-Tpng -oabc.png abc.dot";
            dot.Start();
            dot.WaitForExit();
            return "abc.png";
        }
    }
}
