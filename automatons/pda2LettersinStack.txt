alphabet: abc
stack: xy 
states: S,B,C 
final: C 
transitions: 
S,a [_,x] --> B 
B,b [_,x] --> C 
C,c [y,_] --> S 
end.

dfa: n
finite: n
words:
,n
abc,n
abcab,n
abcc,n
aacc,n
bbbccc,n
aaabbcccc,n
aabbccccc,n
bbaccc,n
aaaabbbacccccccc,n
end.