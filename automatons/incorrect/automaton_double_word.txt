# dfa example
alphabet: 01
states: Z,A,B 
final: A,B 
transitions: 
Z,0 --> A
Z,1 --> B 
A,0 --> A
A,1 --> B
B,0 --> Z
B,1 --> Z 
end.

dfa: y
finite: n
words:
0, y
0, y
1, y
01, y
10, n
end.